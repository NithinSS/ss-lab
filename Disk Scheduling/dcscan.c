#include <stdio.h>
#include <stdlib.h>

#define SEEK_RATE 1
#define ADJ_TIME 20
#define PRINT_HEAD_POS printf( "Head @ track %d\n", head )

// These 2 functions give the algorithm for generating sorted queue
int cmp_dwn ( const void* el1, const void* el2 )
{	
	const int *a = el1, *b = el2 ;
	if ( *a == *b ) return 0 ;
	if ( ( *a < 0 && *b > 0 ) || ( *a > 0 && *b < 0 ) ) return *a > *b ? 1 : -1 ;
	else return *a < *b ? 1 : -1 ;
}

int cmp_up( const void* el1, const void* el2 )
{
	const int *a = el1, *b = el2 ;
	if ( *a == *b ) return 0 ;
	if ( ( *a <= 0 && *b >= 0 ) || ( *a >= 0 && *b <= 0 ) ) return *a < *b ? 1 : -1 ;
	else return abs(*a) > abs(*b) ? 1 : -1 ;
	
}

int main() {
	unsigned int start, head, lim, i = 0, last, THM = 0 , end, prv_head ;
	int *q ;

	printf( "Please enter the limit:" ) ;
	scanf( "%d", &lim) ;
	q = (int*) malloc( lim * sizeof( int ) ) ;
	printf( "Please enter the start head position : " ) ;
	scanf( "%d", &start ) ;
	printf( "Please enter the last track number : " ) ;
	scanf( "%d", &last ) ;
	printf( "Please enter the track locations : \n" ) ;
	while ( i < lim ){
		printf( "Q[%d] : ", i ) ;
		scanf( "%d", &q[i] ) ;
		i++ ;
	}

	// Creating sorted queue 
	for ( i = 0 ; i < lim ; i++ ) {
		q[i] = q[i] - start ;
	}
	if ( start < last / 2 ) { end = 0 ; }
	else { end = last ; }
	if ( end == 0 ) qsort( q , lim, sizeof(int) , cmp_dwn ) ;   // Sorted queue,q1 for : lower end first
	else qsort( q , lim, sizeof(int) , cmp_up ) ; 		// 		    : upper end first

	putchar('\n') ;

	//for( i = 0 ; i < lim ; i++ ) printf( "%d\n", q[i] ) ;
	head = start ;
	PRINT_HEAD_POS ;
	i = 0 ;
	while ( i < lim ) { 
		prv_head = head ;  
		if ( i+1 != lim && (( q[i] > 0 && q[i+1] < 0) || ( q[i] < 0 && q[i+1] > 0 ) )) { 
			head = end ;
			THM += abs( head - prv_head ) ;
			PRINT_HEAD_POS ;
			head = ( end == 0 ) ? last : 0 ;
			PRINT_HEAD_POS ;
			i++;
			continue ;
		}
		else head = q[i] + start ;
		THM += abs( head - prv_head ) ;
		PRINT_HEAD_POS ;
		i++ ;
	}	

	printf( "Total head movement : %d\n", THM );
	printf( "Seek time : %dms\n", THM * SEEK_RATE + ADJ_TIME ) ;
	printf( "Average Seek Time : %.3fms\n", ( (float) THM * SEEK_RATE + ADJ_TIME ) / lim ) ;
	return 0 ;
}
