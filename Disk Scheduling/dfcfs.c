#include <stdio.h>
#include <stdlib.h>

#define SEEK_RATE 1

int main(){
	int start,head,lim,count=0,i,last,THM=0;
	int *q;

	printf("Please enter the limit:");
	scanf("%d",&lim);
	q = (int*)malloc(lim*sizeof(int));
	printf("Please enter the start head position : ");
	scanf("%d",&start);
	printf("Please enter the last track number : ");
	scanf("%d",&last);
	printf("Please enter the track locations : \n");

	while(count < lim){
		printf("Q[%d] : ",count);
		scanf("%d",&q[count]);
		count++;
	}

	putchar('\n');
	head = start;
	for(i=0;i < lim;i++){
			THM += abs(head-q[i]);
			head = q[i];
		}
	free(q);

	printf("Total Head Movement : %d\n",THM);
	printf("Seek time : %d ms\n",THM*SEEK_RATE);
	printf("Average Seek Time : %.3f ms\n",((float)THM*SEEK_RATE)/lim);
		
	return 0;
}
