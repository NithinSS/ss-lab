#include <stdio.h>
#include <stdlib.h>

#define SEEK_RATE 1

int comp0( const void* el1, const void* el2 )
{	
	const int *a = el1, *b = el2 ;
	if ( *a == *b ) return 0 ;
	if (*a < 0 && *b < 0) return *a < *b ? 1 : -1 ;
	else return *a < *b ? -1 : 1 ;
}

int comp1( const void* el1, const void* el2 )
{
	const int *a = el1, *b = el2 ;
	if ( *a == *b ) return 0 ;
	if ( *a > 0 && *b > 0) return *a < *b ? -1 : 1 ;
	else return *a < *b ? 1 : -1 ;
	
}

int main() {
	unsigned int start, head, lim, i = 0, last, THM = 0 , end, phead ;
	int *q, *q1 ;

	printf( "Please enter the limit:" ) ;
	scanf( "%d", &lim) ;
	q = (int*) malloc( lim * sizeof( int ) ) ;
	printf( "Please enter the start head position : " ) ;
	scanf( "%d", &start ) ;
	printf( "Please enter the last track number : " ) ;
	scanf( "%d", &last ) ;
	printf( "Please enter the track locations : \n" ) ;

	while ( i < lim ){
		printf( "Q[%d] : ", i ) ;
		scanf( "%d", &q[i] ) ;
		i++ ;
	}
	q1 = (int*) malloc( lim * sizeof( int ) ) ;
	for ( i = 0 ; i < lim ; i++ ) {
		q1[i] = q[i] - start ;
	}
	q1[lim] = 0 ;
	free( q );
	
	if ( start < last / 2 ) { end = 0 ; }
	else { end = last ; }

	if ( end == 0 ) qsort( q1 , lim + 1 , sizeof(int) , comp0 ) ;   // Sorted queue for : lower end first
	else qsort( q1 , lim + 1, sizeof(int) , comp1 ) ; 		// 		    : upper end first

	putchar('\n') ;
	head = start ;
	printf( "Head @ Track %d\n", head ) ;

	for ( i = 0 ; i < lim + 1 ; i++ ) {
		phead = head ;  
		if ( q1[i] == 0 ) head = end ;
		else head = q1[i] + start ;
		THM += abs( head - phead ) ;
		printf( "Head @ track %d\n", head ) ;
	}	

	printf( "Completed...\n\n" ) ;

	free( q1 ) ;

	printf( "Total Head Movement : %d\n", THM ) ;
	printf( "Seek time : %dms\n", THM * SEEK_RATE ) ;
	printf( "Average Seek Time : %.3fms\n", ( (float) THM * SEEK_RATE ) / lim ) ;
		
	return 0 ;
}
