#include <stdio.h>
#include <stdlib.h>

#define NLINE putchar('\n');
// Process block
typedef struct pcb{
	int pid,bt,done;
}PROC;

int main(){
	int *wt,*tat,limit,ts,time=0,done=0,tmp;
	int i,j;
	PROC* ps;
	float awt=0 , atat=0;
	
	printf("Enter the number of processes : ");
	scanf("%d",&limit);

	printf("Enter the time slice : ");
	scanf("%d",&ts);
	
	ps = (PROC*) malloc(limit * sizeof(PROC));
	wt = (int*) malloc(limit * sizeof(int));
	tat = (int*) calloc(limit,sizeof(int));

	printf("Enter the burst times \n");
	for(i=0 ; i<limit; i++){
		printf("P[%d] : ",i+1);
		scanf("%d",&ps[i].bt);
		ps[i].pid = i+1;
	}
	
	wt[0] = 0;
	for(j=0; j<i ;j++){
		wt[j] = ps[j-1].pid * ts;
		awt+= wt[j];
	}
	
	for(i=0;i<limit;i++)
		ps[i].done = 0;

	NLINE
		printf("---GANTT CHART---");
	NLINE

	while(done!=limit)
	{
		for(i=0;i<limit;i++){
			if(ps[i].done != ps[i].bt){ 
				ps[i].done+=1; 
				if(ps[i].bt-ps[i].done < ts) time+=(ps[i].bt-ps[i].done);
				else time+=ts;	
				printf(" |P[%d]| -> ",i+1); 
			}
			if(ps[i].done == ps[i].bt && tat[i]==0) 
			{ 
				tat[i]=time-ps[i-1].pid; 
				atat+=tat[i]; 
				done++; 
			}
		}	
		
		NLINE
	}

	NLINE

	awt /= limit;
	atat /= limit;
	
	// Printing info
	printf("No.\tBurst\tWait\tTurn Around\n");
	for(i=0 ; i<limit ;i++){
		printf("%3d\t%5d\t%4d\t%10d\n",ps[i].pid,ps[i].bt,wt[i],tat[i]);
	}
	printf("Average Waiting time : %.3f\n",awt);
	printf("Average turn around time : %.3f\n",atat);
	return 0;
}
