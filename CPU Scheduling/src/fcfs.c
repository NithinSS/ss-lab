#include <stdio.h>
#include <stdlib.h>

int main(){
	int *bt,*wt,*tat,limit,i,j;
	float awt=0 , atat=0;
	
	printf("Enter the number of processes : ");
	scanf("%d",&limit);
	
	bt = (int*) malloc(limit * sizeof(int));
	wt = (int*) malloc(limit * sizeof(int));
	tat = (int*) malloc(limit * sizeof(int));

	printf("Enter the burst times \n");
	for(i=0 ; i<limit; i++){
		printf("P[%d] : ",i+1);
		scanf("%d",&bt[i]);
	}

	wt[0] = 0;
	for(j=1; j<i ;j++){
		wt[j] = wt[j-1] + bt[j-1];
		awt+= wt[j];
	//	printf("awt is %f\n",awt);
	}

	for(j=0; j< limit; j++){
		tat[j] = wt[j] + bt[j];
		atat += tat[j];
	}

	awt /= limit;
	atat /= limit;

	printf("No.\tBurst\tWait\tTurn Around\n");
	for(i=0 ; i<limit ;i++){
		printf("%3d\t%5d\t%4d\t%10d\n",i+1,bt[i],wt[i],tat[i]);
	}
	printf("Average Waiting time : %.3f\n",awt);
	printf("Average turn around time : %.3f\n",atat);
	return 0;
}
