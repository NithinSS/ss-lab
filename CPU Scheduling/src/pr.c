#include <stdio.h>
#include <stdlib.h>

// Process block
typedef struct pcb{
	int pid,bt,pr;
}PROC;

// Function to compare burst time, given to qsort()
static int comp(const void * el1, const void * el2){
	const PROC *p1 = el1;
	const PROC *p2 = el2;
	if(p1->pr > p2->pr) return 1;
	if(p1->pr < p2->pr) return -1;
}

int main(){
	int *wt,*tat,limit,i,j;
	PROC* ps;
	float awt=0 , atat=0;
	
	printf("Enter the number of processes : ");
	scanf("%d",&limit);
	
	ps = (PROC*) malloc(limit * sizeof(PROC));
	wt = (int*) malloc(limit * sizeof(int));
	tat = (int*) malloc(limit * sizeof(int));

	printf("Enter the burst times \n");
	for(i=0 ; i<limit; i++){
		printf("P[%d] : ",i+1);
		scanf("%d",&ps[i].bt);
		ps[i].pid = i+1;
	}

	printf("Enter priorities : \n");
	for(i=0 ; i<limit ; i++) {
		printf("P[%d] : ",i+1);
		scanf("%d",&ps[i].pr);
	}
	
	qsort(ps,limit,sizeof(PROC),comp);
	wt[0] = 0;

	
	for(j=1; j<i ;j++){
		wt[j] = wt[j-1] + ps[j-1].bt;
		awt+= wt[j];
	}


	for(j=0; j< limit; j++){
		tat[j] = wt[j] + ps[j].bt;
		atat += tat[j];
	}

	awt /= limit;
	atat /= limit;
	
	// Printing info
	printf("No.\tBurst\tPriority\tWait\tTurn Around\n");
	for(i=0 ; i<limit ;i++){
		printf("%3d\t%5d\t%8d\t%4d\t%10d\n",ps[i].pid,ps[i].bt,ps[i].pr,wt[i],tat[i]);
	}
	printf("Average Waiting time : %.3f\n",awt);
	printf("Average turn around time : %.3f\n",atat);
	return 0;
}
