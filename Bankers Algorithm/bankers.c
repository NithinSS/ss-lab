#include <stdio.h>
#include <stdlib.h>
int main()
{
	unsigned int nores, nopro ,i ,j ; 
	int *available, **allocated, **max, **need ;
	printf( "No of resources : \n" ) ;
	scanf( "%d", &nores ) ;
	available = (int*) malloc( nores * sizeof(int) ) ;
	printf( "Enter available amount of each resources : \n" ) ;
	for ( i = 0 ; i < nores ; i++ ) {
		printf( "R[%d] : ", i+1 ) ;
		scanf( "%d", &available[i] ) ;
	}


	printf( "No of processes : \n" ) ;
	scanf( "%d", &nopro ) ;

	allocated = (int**) malloc( nopro * sizeof(int*) ) ;
	max = (int**) malloc( nopro * sizeof(int*)) ;
	need = (int**) malloc( nopro * sizeof(int*)) ;

	printf( "Enter maximum resources needed for each process\n" ) ;	
	for ( i = 0 ; i < nopro ; i++ ) {
		max[i] = (int*) malloc( nores * sizeof(int) ) ;
		for ( j = 0 ; j < nores ; j++ ) {
			printf( "Maximum R[%d] for P[%d] : ", j+1, i+1 ) ;
			scanf( "%d", &max[i][j] ) ;
			}
		}
	putchar('\n');
	printf( "Enter current allocations\n" ) ;	
	for ( i = 0 ; i < nopro ; i++ ) {
		allocated[i] = (int*) malloc( nores * sizeof(int) ) ;
		for ( j = 0 ; j < nores ; j++ ) {
			printf( "Allocated R[%d] for P[%d] : ", j+1, i+1 ) ;
			scanf( "%d", &allocated[i][j] ) ;
			}
		} 
	putchar('\n');
	
	for ( i = 0 ; i < nopro ; i++ ) {
		need[i] = (int*) malloc( nores * sizeof(int) ) ;
		for ( j = 0 ; j < nores ; j++ ) {
			need[i][j] = max[i][j] - allocated[i][j] ;
			}
		}
	putchar('\n');
	
	printf("\n \t MAX ARRAY \n\n");
	for( i=0,printf("    ") ; i < nores ; i++ ) printf(" R[%d] ",i+1);
	putchar('\n');	
	for( i = 0 ; i < nopro ; i++ ) {
			printf( "P[%d]",i+1) ;
			for( j = 0 ; j < nores ; j++ ) {
				    printf(" %4d ", max[i][j]);
				}
			putchar('\n');
		}

	printf("\n \t ALLOCATED ARRAY \n\n");
	for( i=0,printf("    ") ; i < nores ; i++ ) printf(" R[%d] ",i+1);
	putchar('\n');	
	for( i = 0 ; i < nopro ; i++ ) {
			printf( "P[%d]",i+1) ;
			for( j = 0 ; j < nores ; j++ ) {
				    printf(" %4d ", allocated[i][j]);
				}
			putchar('\n');
		}

	printf("\n \t NEED ARRAY \n\n");
	for( i=0,printf("    ") ; i < nores ; i++ ) printf(" R[%d] ",i+1);
	putchar('\n');	
	for( i = 0 ; i < nopro ; i++ ) {
			printf( "P[%d]",i+1) ;
			for( j = 0 ; j < nores ; j++ ) {
				    printf(" %4d ", need[i][j]);
				}
			putchar('\n');
		}
	free(need) ;
	free(allocated) ;
	free(max) ;
	free(available) ;
	return 0;
}
